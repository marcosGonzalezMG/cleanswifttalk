//
//  AboutWorker.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 08/11/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import FirebaseDatabase

class AboutWorker {

    func doAboutInfoRequest(completionHandler: @escaping (Any?) -> Void) {
        
        let ref = Database.database().reference()
        
        // real time event
        ref.child("about").observe(.value, with: { (aboutInfo) in

            if let value = aboutInfo.value as? [String: Any] {

                completionHandler(value)
            } else {

                completionHandler(nil)
            }
        }, withCancel: { (error) in

            print(error.localizedDescription)
            completionHandler(nil)
        })
        
    //single event
//        ref.child("about").observeSingleEvent(of: .value, with: { (aboutInfo) in
//
//            if let value = aboutInfo.value as? [String:Any] {
//
//                completionHandler(value)
//            } else {
//
//                completionHandler(nil)
//            }
//        }) { (error) in
//
//            print(error.localizedDescription)
//            completionHandler(nil)
//        }
    }
    
    func validateInfo(value: [String: Any]) -> Bool {
        
        if let info = value["about"] {
            
            if (info as? [String: Any]) != nil {
                
                return true
            } else {
                
                return false
            }
        } else {
            
            return false
        }
    }
}
