//
//  AboutView.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 08/11/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//

import UIKit
import Kingfisher

class AboutView: UIView {
    
    let placeholderImage = UIImage(named: "vader")
    
    private struct ViewTraits {

        // Margins
        static let topMargin: CGFloat = 15.0
        static let bottomMargin: CGFloat = 50.0
        static let sideMargin: CGFloat = 20.0
        static let smallMargin: CGFloat = 10.0

        //Heights
        static let viewHeight: CGFloat = 150.0
        static let imageHeight: CGFloat = 250.0
        static let iconSize: CGFloat = 30.0
        static let barSize: CGFloat = 60.0
    }

    private let simpleView: UIView
    let imageView: UIImageView
    let scrollImageView: UIScrollView
    private let titleBarView: UIView
    private let swiftIconView: UIImageView
    private let titleLabel: UILabel
    private let bodyTextView: UITextView

    override init(frame: CGRect) {

        //simpleView
        simpleView = UIView()
        simpleView.backgroundColor = .clear
        
        //imageView
        imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isHidden = true
        
        //scrollImageView
        scrollImageView = UIScrollView()
        scrollImageView.alwaysBounceVertical = false
        scrollImageView.alwaysBounceHorizontal = false
        scrollImageView.bounces = false
        scrollImageView.bouncesZoom = false
        scrollImageView.showsVerticalScrollIndicator = false
        scrollImageView.minimumZoomScale = 1.0
        scrollImageView.maximumZoomScale = 10.0
        scrollImageView.clipsToBounds = true
    
        //titleBarView
        titleBarView = UIView()
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = titleBarView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.alpha = 0.9
        titleBarView.addSubview(blurEffectView)
        titleBarView.isHidden = true
        
        //swiftIconView
        swiftIconView = UIImageView(image: UIImage(named: "swift"))
        swiftIconView.contentMode = .scaleAspectFill
        swiftIconView.clipsToBounds = true
        
        //titleLabel
        titleLabel = UILabel()
        titleLabel.textColor = .white
        titleLabel.textAlignment = .left
        titleLabel.font = UIFont.systemFont(ofSize: 22.0)
        
        //bodyTextView
        bodyTextView = UITextView()
        bodyTextView.isEditable = false
        bodyTextView.showsHorizontalScrollIndicator = false
        bodyTextView.font = UIFont.systemFont(ofSize: 20.0)

        // Init
        super.init(frame: frame)

        backgroundColor = .white

        // Add subviews
        addSubview(simpleView)
        simpleView.addSubview(scrollImageView)
        scrollImageView.addSubview(imageView)
        simpleView.addSubview(titleBarView)
        titleBarView.addSubview(swiftIconView)
        titleBarView.addSubview(titleLabel)
        simpleView.addSubview(bodyTextView)
        
        // Add constraints
        simpleView.translatesAutoresizingMaskIntoConstraints = false
        scrollImageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false
        titleBarView.translatesAutoresizingMaskIntoConstraints = false
        swiftIconView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        bodyTextView.translatesAutoresizingMaskIntoConstraints = false

        addCustomConstraints()
    }

    required init?(coder aDecoder: NSCoder) {

        fatalError("init(coder:) has not been implemented")
    }

    private func addCustomConstraints() {
        
        let scrollWidth = UIScreen.main.bounds.width

        NSLayoutConstraint.activate([

            // Horizontal
            simpleView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            simpleView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            scrollImageView.leadingAnchor.constraint(equalTo: simpleView.leadingAnchor),
            scrollImageView.trailingAnchor.constraint(equalTo: simpleView.trailingAnchor),
            imageView.leadingAnchor.constraint(equalTo: scrollImageView.leadingAnchor),
            imageView.widthAnchor.constraint(equalToConstant: scrollWidth),
            titleBarView.leadingAnchor.constraint(equalTo: simpleView.leadingAnchor),
            titleBarView.trailingAnchor.constraint(equalTo: simpleView.trailingAnchor),
            swiftIconView.leadingAnchor.constraint(equalTo: titleBarView.leadingAnchor, constant: ViewTraits.sideMargin),
            titleLabel.leadingAnchor.constraint(equalTo: swiftIconView.trailingAnchor, constant: ViewTraits.smallMargin),
            titleLabel.trailingAnchor.constraint(equalTo: titleBarView.trailingAnchor, constant: -ViewTraits.sideMargin),
            bodyTextView.leadingAnchor.constraint(equalTo: simpleView.leadingAnchor, constant: ViewTraits.sideMargin),
            bodyTextView.trailingAnchor.constraint(equalTo: simpleView.trailingAnchor, constant: -ViewTraits.sideMargin),

            // Vertical
            simpleView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            simpleView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            scrollImageView.topAnchor.constraint(equalTo: simpleView.topAnchor),
            scrollImageView.heightAnchor.constraint(equalToConstant: ViewTraits.imageHeight),
            imageView.topAnchor.constraint(equalTo: scrollImageView.topAnchor),
            imageView.bottomAnchor.constraint(equalTo: scrollImageView.bottomAnchor),
            imageView.heightAnchor.constraint(equalToConstant: ViewTraits.imageHeight),
            titleBarView.bottomAnchor.constraint(equalTo: scrollImageView.bottomAnchor),
            titleBarView.heightAnchor.constraint(equalToConstant: ViewTraits.barSize),
            swiftIconView.centerYAnchor.constraint(equalTo: titleBarView.centerYAnchor),
            swiftIconView.heightAnchor.constraint(equalToConstant: ViewTraits.iconSize),
            swiftIconView.widthAnchor.constraint(equalToConstant: ViewTraits.iconSize),
            titleLabel.centerYAnchor.constraint(equalTo: titleBarView.centerYAnchor),
            bodyTextView.topAnchor.constraint(equalTo: titleBarView.bottomAnchor, constant: ViewTraits.sideMargin),
            bodyTextView.bottomAnchor.constraint(equalTo: simpleView.bottomAnchor, constant: -ViewTraits.sideMargin)
        ])

    }
    
    func setTitle(title: String) {
        
        titleLabel.text = title
    }
    
    func setBody(body: String) {
        
        bodyTextView.text = body
    }
    
    func hideTitleBar(_ hide: Bool) {
        
        titleBarView.isHidden = hide
    }
}
