//
//  ResourceDetailView.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 09/11/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//

import UIKit

class ResourceDetailView: UIView {

    private struct ViewTraits {

        // Margins
        static let topMargin: CGFloat = 25.0
        static let sideMargin: CGFloat = 15.0
        static let innerMargin: CGFloat = 10.0
        static let bottomMargin: CGFloat = 15.0

        //height
        static let textfieldHeight: CGFloat = 50.0
        static let buttonHeight: CGFloat = 50.0
        static let labelHeight: CGFloat = 35.0

        //width
        static let buttonWidth: CGFloat = 40.0
        static let labelWidth: CGFloat = 50.0
    }

    // MARK: Public
    let titleLabelDescription: UILabel
    let nameLabel: UILabel
    let itemDescTitleLabel: UILabel
    let itemDescription: UILabel

    override init(frame: CGRect) {

        //itemDescTitleLabel
        titleLabelDescription = UILabel()
        titleLabelDescription.text = NSLocalizedString("Name:", comment: String(describing: ResourceDetailView.self))
        titleLabelDescription.textColor = .customOrange

        //nameLabel
        nameLabel = UILabel()
        nameLabel.numberOfLines = 2

        //itemDescTitleLabel
        itemDescTitleLabel = UILabel()
        itemDescTitleLabel.text = NSLocalizedString("Description:", comment: String(describing: ResourceDetailView.self))
        itemDescTitleLabel.textColor = .customOrange

        //itemDescription
        itemDescription = UILabel()
        itemDescription.numberOfLines = 0

        // Init
        super.init(frame: frame)

        backgroundColor = .white

        // Add subviews
        addSubview(titleLabelDescription)
        addSubview(nameLabel)
        addSubview(itemDescTitleLabel)
        addSubview(itemDescription)

        // Add constraints
        titleLabelDescription.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        itemDescTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        itemDescription.translatesAutoresizingMaskIntoConstraints = false

        addCustomConstraints()
    }

    required init?(coder aDecoder: NSCoder) {

        fatalError("init(coder:) has not been implemented")
    }

    private func addCustomConstraints() {

        NSLayoutConstraint.activate([

            // Horizontal
            titleLabelDescription.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: ViewTraits.sideMargin),
            titleLabelDescription.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -ViewTraits.sideMargin),
            nameLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: ViewTraits.sideMargin),
            nameLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -ViewTraits.sideMargin),
            itemDescTitleLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: ViewTraits.sideMargin),
            itemDescTitleLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -ViewTraits.sideMargin),
            itemDescription.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: ViewTraits.sideMargin),
            itemDescription.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -ViewTraits.sideMargin),

            // Vertical
            titleLabelDescription.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: ViewTraits.topMargin),
            titleLabelDescription.heightAnchor.constraint(equalToConstant: ViewTraits.labelHeight),
            nameLabel.topAnchor.constraint(equalTo: titleLabelDescription.bottomAnchor, constant: ViewTraits.innerMargin),
            itemDescTitleLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: ViewTraits.innerMargin),
            itemDescTitleLabel.heightAnchor.constraint(equalToConstant: ViewTraits.labelHeight),
            itemDescription.topAnchor.constraint(equalTo: itemDescTitleLabel.bottomAnchor, constant: ViewTraits.innerMargin),
            itemDescription.bottomAnchor.constraint(lessThanOrEqualTo: safeAreaLayoutGuide.bottomAnchor, constant: -ViewTraits.bottomMargin)
        ])
    }
}
