//
//  ResourceDetailInteractor.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 09/11/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol ResourceDetailBusinessLogic {

    func retrieveItemInformation(request: ResourceDetail.Information.Request)
}

protocol ResourceDetailDataStore {

    var itemToShow: [String: Any] { get set }
}

class ResourceDetailInteractor: ResourceDetailBusinessLogic, ResourceDetailDataStore {

    var presenter: ResourceDetailPresentationLogic?
    var worker: ResourceDetailWorker?
    var itemToShow = [String: Any]()

    // MARK: Do something
    func retrieveItemInformation(request: ResourceDetail.Information.Request) {

        let response = ResourceDetail.Information.Response(itemData: itemToShow)
        presenter?.presentItemInformation(response: response)
    }
}
