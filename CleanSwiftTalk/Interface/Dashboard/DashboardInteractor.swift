//
//  DashboardInteractor.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 01/11/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol DashboardBusinessLogic {

    func getCatalog(request: Dashboard.Catalog.Request)
}

protocol DashboardDataStore {

    var catalog: [String: Any] { get set }
}

class DashboardInteractor: DashboardBusinessLogic, DashboardDataStore {

    var presenter: DashboardPresentationLogic?
    var worker: DashboardWorker?
    var catalog = [String: Any]()

    // MARK: Do something
    func getCatalog(request: Dashboard.Catalog.Request) {

        worker = DashboardWorker()
        worker?.doCatalogRequest(completionHandler: { [weak self] (responseResult) in

            var response: Dashboard.Catalog.Response

            switch responseResult {

            case .successDict(let dictResponse):

//                let data = NSKeyedArchiver.archivedData(withRootObject: dictResponse)

                self?.catalog = dictResponse
                response = Dashboard.Catalog.Response.success(responseObj: dictResponse)
            case .error(let error):

                response = Dashboard.Catalog.Response.failure(error: error)
            default:

                let unexpectedError = NSError(domain: ErrorDomain.Generic,
                                              code: Custom.ErrorType.unexpectedResponseFormat.rawValue,
                                              userInfo: [NSLocalizedDescriptionKey: Custom.ErrorType.localizedString(forErrorCode: Custom.ErrorType.unexpectedResponseFormat)])
                response = Dashboard.Catalog.Response.failure(error: unexpectedError)
            }

            self?.presenter?.presentCatalogResponse(response: response)
        })
    }
}
