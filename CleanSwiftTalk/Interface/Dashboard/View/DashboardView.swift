//
//  DashboardView.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 09/11/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//

import UIKit

class DashboardView: UIView {

    private struct ViewTraits {

        // Margins
        static let topMargin: CGFloat = 15.0
        static let bottomMargin: CGFloat = 50.0

        //Heights
        static let viewHeight: CGFloat = 150.0
    }

    // MARK: Public
    let tableView: UITableView

    override init(frame: CGRect) {

        //tableView
        tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .clear

        // Init
        super.init(frame: frame)

        backgroundColor = .white

        // Add subviews
        addSubview(tableView)

        // Add constraints
        tableView.translatesAutoresizingMaskIntoConstraints = false

        addCustomConstraints()
    }

    required init?(coder aDecoder: NSCoder) {

        fatalError("init(coder:) has not been implemented")
    }

    private func addCustomConstraints() {

        NSLayoutConstraint.activate([

            // Horizontal
            tableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),

            // Vertical
            tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}
