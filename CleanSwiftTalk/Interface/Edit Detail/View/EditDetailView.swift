//
//  EditDetailView.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 22/11/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//

import UIKit

class EditDetailView: UIView {

    private struct ViewTraits {

        // Margins
        static let topMargin: CGFloat = 25.0
        static let sideMargin: CGFloat = 15.0
        static let bottomMargin: CGFloat = 50.0
        static let innerMargin: CGFloat = 10.0

        //height
        static let textfieldHeight: CGFloat = 50.0
        static let labelHeight: CGFloat = 35.0
        static let lineHeight: CGFloat = 1.0

        //width
        static let buttonWidth: CGFloat = 60.0
    }

    // MARK: Public
    let editLabelDescription: UILabel
    let toEditTF: UITextField
    let lineView: UIView
    let saveButton: UIButton

    override init(frame: CGRect) {

        //itemDescTitleLabel
        editLabelDescription = UILabel()
        editLabelDescription.text = NSLocalizedString("New name:", comment: String(describing: EditDetailView.self))
        editLabelDescription.textColor = .customOrange

        //nameTF
        toEditTF = UITextField()
        toEditTF.returnKeyType = .done
        toEditTF.tintColor = .customOrange

        //lineView
        lineView = UIView()
        lineView.backgroundColor = .customOrange

        //saveButton
        saveButton = UIButton(type: .roundedRect)
        saveButton.setTitle(NSLocalizedString("Save", comment: String(describing: EditDetailView.self)), for: UIControl.State())
        saveButton.tintColor = .customOrange
        saveButton.isEnabled = false

        // Init
        super.init(frame: frame)

        backgroundColor = .white

        // Add subviews
        addSubview(editLabelDescription)
        addSubview(toEditTF)
        addSubview(saveButton)
        addSubview(lineView)

        // Add constraints
        editLabelDescription.translatesAutoresizingMaskIntoConstraints = false
        toEditTF.translatesAutoresizingMaskIntoConstraints = false
        saveButton.translatesAutoresizingMaskIntoConstraints = false
        lineView.translatesAutoresizingMaskIntoConstraints = false

        addCustomConstraints()
    }

    required init?(coder aDecoder: NSCoder) {

        fatalError("init(coder:) has not been implemented")
    }

    private func addCustomConstraints() {

        NSLayoutConstraint.activate([

            // Horizontal
            editLabelDescription.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: ViewTraits.sideMargin),
            editLabelDescription.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -ViewTraits.sideMargin),
            toEditTF.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: ViewTraits.sideMargin),
            saveButton.leadingAnchor.constraint(equalTo: toEditTF.trailingAnchor, constant: ViewTraits.sideMargin),
            saveButton.widthAnchor.constraint(equalToConstant: ViewTraits.buttonWidth),
            saveButton.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -ViewTraits.sideMargin),
            lineView.leadingAnchor.constraint(equalTo: toEditTF.leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: toEditTF.trailingAnchor),

            // Vertical
            editLabelDescription.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: ViewTraits.topMargin),
            editLabelDescription.heightAnchor.constraint(equalToConstant: ViewTraits.labelHeight),
            toEditTF.topAnchor.constraint(equalTo: editLabelDescription.bottomAnchor, constant: ViewTraits.innerMargin),
            toEditTF.heightAnchor.constraint(equalToConstant: ViewTraits.textfieldHeight),
            saveButton.topAnchor.constraint(equalTo: toEditTF.topAnchor),
            saveButton.bottomAnchor.constraint(equalTo: toEditTF.bottomAnchor),
            lineView.topAnchor.constraint(equalTo: toEditTF.bottomAnchor),
            lineView.heightAnchor.constraint(equalToConstant: ViewTraits.lineHeight)
        ])
    }
}
