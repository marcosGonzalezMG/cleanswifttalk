//
//  EditDetailInteractor.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 22/11/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol EditDetailBusinessLogic {

    func retrieveInfoToEdit(request: EditDetail.Edit.Request)
    func saveItemName(request: EditDetail.Save.Request)
}

protocol EditDetailDataStore {

    var itemToEdit: [String: Any] { get set }
}

class EditDetailInteractor: EditDetailBusinessLogic, EditDetailDataStore {

    var presenter: EditDetailPresentationLogic?
    var worker: EditDetailWorker?
    var itemToEdit = [String: Any]()

    // MARK: Do something
    func retrieveInfoToEdit(request: EditDetail.Edit.Request) {

        let response = EditDetail.Edit.Response(itemToEdit: itemToEdit)
        presenter?.presentItemToEdit(response: response)
    }

    func saveItemName(request: EditDetail.Save.Request) {

        var saved = false
        if itemToEdit["name"] as? String != nil {

            itemToEdit["name"] = request.editedInfo
            saved = true
        } else if itemToEdit["title"] as? String != nil {

            itemToEdit["name"] = request.editedInfo
            saved = true
        }

        let response = EditDetail.Save.Response(saved: saved, itemName: request.editedInfo)
        presenter?.presentItemSaved(response: response)
    }
}
