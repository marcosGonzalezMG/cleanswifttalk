//
//  RootWindowRouting.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 15/11/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//

import Foundation
import UIKit

protocol KeyWindowRouting {

    func dashboardAsRootViewcontroller()
    func loginAsRootViewcontroller()
}

extension KeyWindowRouting {

    func dashboardAsRootViewcontroller() {

        let tabBarViewController = UITabBarController()
        let dashboardVC = DashboardViewController()
        dashboardVC.tabBarItem = UITabBarItem(title: NSLocalizedString("Catalog", comment: "Tab bar item"), image: UIImage(named: "catalog"), tag: 0)
        let optionsVC = OptionsViewController()
        optionsVC.tabBarItem = UITabBarItem(title: NSLocalizedString("Options", comment: "Tab bar item"), image: UIImage(named: "options"), tag: 1)

        let tabBarControllers = [dashboardVC, optionsVC]
        tabBarViewController.viewControllers = tabBarControllers.map {
            let navVC = UINavigationController(rootViewController: $0)
            navVC.navigationBar.prefersLargeTitles = true
            return navVC
        }

        tabBarViewController.selectedIndex = 0
        tabBarViewController.tabBar.tintColor = .customOrange

        UIApplication.shared.keyWindow?.rootViewController = tabBarViewController
    }

    func loginAsRootViewcontroller() {

        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let stotyboarVC = mainStoryboard.instantiateViewController(withIdentifier: "LoginUserViewController")
        let navigationController = UINavigationController(rootViewController: stotyboarVC)
        navigationController.navigationBar.prefersLargeTitles = true

        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
}
