//
//  CustomLog.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 24/10/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//

import Foundation

public func DLog(_ message: String, function: String = #function) {

    #if DEBUG
        print("\(function): \(message)")
    #endif
}
