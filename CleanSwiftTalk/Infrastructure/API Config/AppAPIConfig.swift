//
//  AppAPIConfig.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 24/10/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//

import Foundation

struct AppConfig {

    // MARK: API BASE URLs:

    // MARK: App API
    internal struct APIUrl {

        static let baseUrl = "swapi.co/api"
        static let urlString = "https://\(baseUrl)"
    }

//// MARK: API KEYS:
//// MARK: APP Auth API
//    internal struct APIAuthKeys {
//
//        static let clientID = ""
//        static let clientSecret = ""
//
//        static let authToken = ""
//        static let authorization = "Authorization"
//    }
}
