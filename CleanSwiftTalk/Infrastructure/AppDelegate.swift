//
//  AppDelegate.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 24/10/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        FirebaseApp.configure()

        UINavigationBar.appearance().tintColor = .customOrange

        // Window stack
        var rootViewController: UIViewController

        if AppSettings.current().isUserLoggedIn {

            let tabBarViewController = UITabBarController()
            let dashboardVC = DashboardViewController()
            dashboardVC.tabBarItem = UITabBarItem(title: NSLocalizedString("Catalog", comment: "Tab bar item"), image: UIImage(named: "catalog"), tag: 0)
            let optionsVC = OptionsViewController()
            optionsVC.tabBarItem = UITabBarItem(title: NSLocalizedString("Options", comment: "Tab bar item"), image: UIImage(named: "options"), tag: 1)

            let tabBarControllers = [dashboardVC, optionsVC]
            tabBarViewController.viewControllers = tabBarControllers.map {
                let navVC = UINavigationController(rootViewController: $0)
                navVC.navigationBar.prefersLargeTitles = true

                return navVC
            }

            tabBarViewController.selectedIndex = 0
            tabBarViewController.tabBar.tintColor = .customOrange

            rootViewController = tabBarViewController
        } else {

            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let stotyboarVC = mainStoryboard.instantiateViewController(withIdentifier: "LoginUserViewController")
            let navigationController = UINavigationController(rootViewController: stotyboarVC)
            navigationController.navigationBar.prefersLargeTitles = true

            rootViewController = navigationController
        }

        self.window?.rootViewController = rootViewController
        self.window?.makeKeyAndVisible()

        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        CoreDataManager.shared.save()
    }
}
