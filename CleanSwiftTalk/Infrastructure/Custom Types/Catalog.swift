//
//  Catalog.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 09/11/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//

import Foundation

struct Catalog {

    enum Categories: String {

        case none
        case vehicles
        case planets
        case starships
        case people
        case films
        case species

        static func from(string: String) -> Categories {

            return Categories(rawValue: string.lowercased()) ?? .none
        }
    }
}
