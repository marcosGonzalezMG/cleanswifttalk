//
//  ColorExtensions.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 08/11/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//

import UIKit

typealias Color = UIColor

extension UIColor {

    static let customOrange = UIColor(red: 0.996, green: 0.317, blue: 0.003, alpha: 1.0)
    static let customLightOrange = UIColor(red: 1.0, green: 0.811, blue: 0.678, alpha: 1.0)
}
