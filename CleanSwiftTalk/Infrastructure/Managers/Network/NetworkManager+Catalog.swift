//
//  NetworkManager+Catalog.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 26/10/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//

import Foundation

extension NetworkManager {

    internal func getCatalog(completionHandler: @escaping NetworkManagerCompletionHandler) {

        request(router: Router.getCatalog, completionHandler: completionHandler)
    }
}
