//
//  NetworkManager+AnyResource.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 09/11/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//

import Foundation

extension NetworkManager {

    internal func getAnyResource(withPath pathString: String, completionHandler: @escaping NetworkManagerCompletionHandler) {

        request(router: Router.getResource(path: pathString), completionHandler: completionHandler)
    }
}
