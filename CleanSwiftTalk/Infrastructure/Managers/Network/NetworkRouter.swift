//
//  NetworkRouter.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 24/10/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//

import Foundation
import Alamofire

enum Router {

    case getCatalog
    case getResource(path: String)
//    case loginUser(credentialsDict: [String: String])
}

protocol ContextProvider {

    var context: String { get }
    func asURLRequestAndParams() throws -> (URLRequest, Parameters?)
}

// MARK: URLRequestConvertible
extension Router: URLRequestConvertible {

    private static let baseURLString = AppConfig.APIUrl.urlString

    private struct Endpoint {

        static let catalog = "/"
    }

    private func asHttpMethod() -> HTTPMethod {

        switch self {

        case .getCatalog:
            return .get
        case .getResource:
            return .get
//        case .loginUser: return .post
        }
    }

    private var encoding: ParameterEncoding {

        switch self {

        case .getResource:

            return URLEncoding.default
        default:
            return JSONEncoding.default
        }
    }

    func asURLRequest() throws -> URLRequest {

        let (urlRequest, parameters) = try asURLRequestAndParams()

        return try encoding.encode(urlRequest, with: parameters)
    }
}

extension Router: ContextProvider {

    var context: String {

        switch self {

        case .getCatalog:
            return "getCatalog"
        case .getResource:
            return "getResource"
        }
    }

    func asURLRequestAndParams() throws -> (URLRequest, Parameters?) {

        let result: (path: String, parameters: Parameters?) = {

            switch self {

            case .getCatalog:

                return (Endpoint.catalog, nil)
            case .getResource(let endpointPath):

                return (endpointPath, nil)
            }
        }()

        let url = try Router.baseURLString.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
        urlRequest.httpMethod = asHttpMethod().rawValue
        urlRequest.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")

        return (urlRequest, result.parameters)
    }
}
