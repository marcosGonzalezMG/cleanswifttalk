//
//  CoreDataManager.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 24/10/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//

import CoreData

class CoreDataManager {

    private struct AppCoreDataConfig {

        static let modelURL = "CleanSwiftTalk"
        static let modelURLExtension = "momd"
        static let persistenceStore = "CleanSwiftTalk.sqlite"
    }

    static let shared = CoreDataManager()

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: URL = {

        // The directory which the application uses to store the Core Data store file. This code uses a directory in the application's documents Application Support directory.
        //.ApplicationSupportDirectory
        var dbFileURL: URL
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .applicationSupportDirectory, in: .userDomainMask)

        do {

            guard let url = urls.last else {

                return createLibraryDirectory()
            }

            try fileManager.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
            dbFileURL = url
        } catch {

            dbFileURL = createLibraryDirectory()
        }

        return dbFileURL
    }()

    private func createLibraryDirectory() -> URL {

        let fileManager = FileManager.default
        let ifFailUrls = fileManager.urls(for: .libraryDirectory, in: .userDomainMask)

        guard let url = ifFailUrls.last else {

            // This property is not optional. It is a fatal error for the application not to be able to find and load library directory.
            fatalError("No library directory found")
        }

        return url
    }

    // The managed object model for the application.
    lazy var managedObjectModel: NSManagedObjectModel = {

        let bundle = Bundle(for: type(of: self))
        guard let modelURL = bundle.url(forResource: AppCoreDataConfig.modelURL, withExtension: AppCoreDataConfig.modelURLExtension),
            let model = NSManagedObjectModel(contentsOf: modelURL)
            else {

                // This property is not optional. It is a fatal error for the application not to be able to find and load its model.
                fatalError("Missing data model")
        }

        return model
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {

        // The persistent store coordinator for the application. This implementation creates and returns a coordinator,
        // having added the store for the application to it. This property is optional since there are legitimate error
        // conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        let url = applicationDocumentsDirectory.appendingPathComponent(AppCoreDataConfig.persistenceStore)

        let options = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
        do {

            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
        } catch {

            // Report any error we got.
            var dict = [String: Any]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = "There was an error creating or loading the application's saved data."
            dict[NSUnderlyingErrorKey] = error as NSError

            let wrappedError = NSError(domain: "com.mobgen.error.domain", code: 9999, userInfo: dict)

            DLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            DLog("==> Wiping out the database")
            // abort() causes the application to generate a crash log and terminate. You should not use this function in
            // a shipping application, although it may be useful during development.

            do {

                try FileManager.default.removeItem(at: url)

                do {

                    try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
                } catch {

                    let cError = error as NSError
                    DLog("Creating new db error: \(cError.localizedDescription)")
                }
            } catch {

                let cError = error as NSError
                DLog("Removing old db error: \(cError.localizedDescription)")
            }
        }

        //exclude this file from iCloud automatic backups
        do {

            let fileValuesOption = [URLResourceKey.isExcludedFromBackupKey: true]

            try (url as NSURL).setResourceValues(fileValuesOption)
        } catch let error {
            // do nothing
            DLog("Error set skip from backup value: \(error)")
        }

        //set protection level to complete
        let fileAttributes = [FileAttributeKey.protectionKey: FileProtectionType.complete]
        do {

            let path = url.path
            try FileManager.default.setAttributes(fileAttributes, ofItemAtPath: path)
        } catch {

            let cError = error as NSError
            DLog("Protecting file error: \(cError.localizedDescription)")
        }

        return coordinator
    }()

    lazy var mainObjectContext: NSManagedObjectContext = {

        // Returns the managed object context for the application (which is already bound to the
        // persistent store coordinator for the application.) This property is optional since there are legitimate
        // error conditions that could cause the creation of the context to fail.
        let coordinator = persistentStoreCoordinator
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator

        return managedObjectContext
    }()
}

extension CoreDataManager {

    // MARK: - Core Data Saving support

    func saveContext(_ context: NSManagedObjectContext) {

        if context.hasChanges {

            do {

                try context.save()
            } catch {

                let nserror = error as NSError
                fatalError("Error saving context \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func save() {

        saveContext(mainObjectContext)
    }
}

extension CoreDataManager {

    // MARK: - Delete methods

    func deleteObject(_ object: NSManagedObject, context: NSManagedObjectContext) {

        context.delete(object)
    }

    func deleteObject(_ object: NSManagedObject) {

        deleteObject(object, context: mainObjectContext)
    }
}
