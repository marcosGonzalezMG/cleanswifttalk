//
//  ManagedObject.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 30/10/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//

import CoreData

protocol ManagedObjectProtocol {

    func fromDictionary(_ values: [String: Any])
    func toDictionary() -> [String: Any]
}

extension NSManagedObject {

    struct Properties {

        static let objectIDKey = "oid"
    }

    // MARK: - Query functions
    class func queryForAll() -> [NSManagedObject]? {

        return queryForAll(CoreDataManager.shared.mainObjectContext)
    }

    class func queryForAll(_ context: NSManagedObjectContext) -> [NSManagedObject]? {

        return queryForPredicate(nil, sortDescriptors: nil, fetchLimit: 0, context: context)
    }

    class func queryForPredicate(_ predicate: NSPredicate?) -> [NSManagedObject]? {

        return queryForPredicate(predicate, context: CoreDataManager.shared.mainObjectContext)
    }

    class func queryForPredicate(_ predicate: NSPredicate?, context: NSManagedObjectContext) -> [NSManagedObject]? {

        return queryForPredicate(predicate, sortDescriptors: [], fetchLimit: 0, context: context)
    }

    class func queryForPredicate(_ predicate: NSPredicate?, sortDescriptor: NSSortDescriptor?) -> [NSManagedObject]? {

        return queryForPredicate(predicate, sortDescriptor: sortDescriptor, context: CoreDataManager.shared.mainObjectContext)
    }

    class func queryForPredicate(_ predicate: NSPredicate?, sortDescriptor: NSSortDescriptor?, context: NSManagedObjectContext) -> [NSManagedObject]? {

        return queryForPredicate(predicate, sortDescriptor: sortDescriptor, fetchLimit: 0, context: context)
    }

    class func queryForPredicate(_ predicate: NSPredicate?, sortDescriptor: NSSortDescriptor?, fetchLimit: Int, context: NSManagedObjectContext) -> [NSManagedObject]? {

        let descriptorsArray: [NSSortDescriptor]?
        if let descriptor = sortDescriptor {

            descriptorsArray = [descriptor]
        } else {

            descriptorsArray = nil
        }

        return queryForPredicate(predicate, sortDescriptors: descriptorsArray, fetchLimit: fetchLimit, context: context)
    }

    class func queryForPredicate(_ predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor]?) -> [NSManagedObject]? {

        return queryForPredicate(predicate, sortDescriptors: sortDescriptors, context: CoreDataManager.shared.mainObjectContext)
    }

    class func queryForPredicate(_ predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor]?, context: NSManagedObjectContext) -> [NSManagedObject]? {

        return queryForPredicate(predicate, sortDescriptors: sortDescriptors, fetchLimit: 0, context: context)
    }

    class func queryForPredicate(_ predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor]?, fetchLimit: Int, context: NSManagedObjectContext) -> [NSManagedObject]? {

        let className = String(describing: self)

        let request = NSFetchRequest<NSFetchRequestResult>()
        request.entity = NSEntityDescription.entity(forEntityName: className, in: context)
        request.predicate = predicate
        request.sortDescriptors = sortDescriptors

        if fetchLimit > 0 {

            request.fetchLimit = fetchLimit
        }

        do {

            let objects = try context.fetch(request) as? [NSManagedObject]

            return objects
        } catch {

            let cError = error as NSError
            DLog("Fetching db error: \(cError.localizedDescription)")
            return nil
        }
    }

    class func queryForIdentifier(_ identifier: Any) -> NSManagedObject? {

        return queryForIdentifier(identifier, context: CoreDataManager.shared.mainObjectContext)
    }

    class func queryForIdentifier(_ identifier: Any, context: NSManagedObjectContext) -> NSManagedObject? {

        let predicate = NSPredicate(format: "%@ == %@", argumentArray: [Properties.objectIDKey, identifier])
        let valueObjects = queryForPredicate(predicate, context: context)

        guard let values = valueObjects else {
            return nil
        }

        return values.last
    }

    // MARK: - Delete
    func deleteFromContext() {

        managedObjectContext?.delete(self)
    }
}
