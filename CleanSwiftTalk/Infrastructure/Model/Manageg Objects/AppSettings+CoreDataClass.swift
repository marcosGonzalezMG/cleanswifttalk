//
//  AppSettings+CoreDataClass.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 30/10/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//
//

import Foundation
import CoreData

@objc(AppSettings)
class AppSettings: NSManagedObject {

    struct ServerJSonKeys {

        static let isUserLogged = "isUserLoggedIn"
    }

    // MARK: - Factory functions
    class func createOrUpdateWithDictionary(values: [String: Any]?, context: NSManagedObjectContext = CoreDataManager.shared.mainObjectContext) -> NSManagedObject? {

        var object = queryForAll(context)?.last

        if object == nil {

            object = NSEntityDescription.insertNewObject(forEntityName: String(describing: self), into: context)
        }

        guard let obj = object as? AppSettings else {

            return nil
        }

        obj.fromDictionary(values ?? [:])

        return obj
    }

    class func current() -> AppSettings {

        let managedOject = queryForAll()?.last as? AppSettings

        guard let obj = managedOject else {

            if let newObj = createOrUpdateWithDictionary(values: nil) as? AppSettings {

                return newObj
            } else {

                // cant initialize app settings. App should die!
                assert(false, "Entity representation not found")
                return AppSettings()
            }
        }

        return obj
    }

    class func delete() {

        let managedOject = queryForAll(CoreDataManager.shared.mainObjectContext)?.last as? AppSettings

        if let obj = managedOject {

            CoreDataManager.shared.mainObjectContext.delete(obj)
        }
    }

    class func save() {

        CoreDataManager.shared.save()
    }
}

extension AppSettings: ManagedObjectProtocol {

    func fromDictionary(_ values: [String: Any] = [:]) {

        if let sIsUserLoggedIn = values[ServerJSonKeys.isUserLogged] as? Bool {
            isUserLoggedIn = sIsUserLoggedIn
        }
    }

    func toDictionary() -> [String: Any] {

        var toret = [String: Any]()

        toret[ServerJSonKeys.isUserLogged] = isUserLoggedIn

        return toret
    }
}
