//
//  AppSettings+CoreDataProperties.swift
//  CleanSwiftTalk
//
//  Created by Marcos Gonzalez on 30/10/2017.
//  Copyright © 2017 MOBGEN. All rights reserved.
//
//

import Foundation
import CoreData

extension AppSettings {

    @NSManaged var isUserLoggedIn: Bool
}
